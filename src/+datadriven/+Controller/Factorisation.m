classdef Factorisation < int8
    %FACTORISATION Represent controller factorisations with integer values
    %   This class sets up the interface for the Controller optimisation
    % 
    %   FACTORISATION enumeration members:
    %   * Left  - represents 'Left' factorisation, or integer 0
    %   * Right - represents 'Right' factorisation, or integer 1
    % 
    %   ------------------------------------------------------------------------
    %   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
    %
    
    enumeration
        Left (0)
        Right (1)
    end
end

