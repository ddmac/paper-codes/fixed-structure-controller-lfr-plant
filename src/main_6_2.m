%% Script to generate results from Section 6.2 of the paper
% 
% References
%   [1] P. Schuchert, V. Gupta, and A. Karimi, “Data-driven fixed-structure 
%   frequency-based $\mathcal{H}_2$ and $\mathcal{H}_\infty$ controller design,”
%   Automatica, vol. 160, p. 111398, Feb. 2024, 
%   doi: 10.1016/j.automatica.2023.111398.
% 
% ------------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%

clc;
clearvars;
close all;

%% Options
examples_to_run = [ ...
    "AC1" , "AC2", "AC3", "AC5", ...
    "HE1" , ...
    "JE1" , ...
    "REA1", ...
    "DIS1", "DIS2", ...
    "TG1", ...
    "AGS" , ...
    "WEC1", "WEC2", ...
    "HF1" , ...
    "BDT1", ...
    "MFP", ...
    "PSM"];

%% $H_\infty$ case (Table 2)
outputs = struct();
failed = [];
for example = examples_to_run
    clear('yalmip');    % Cleanup YALMIP backend
    try
        outputs.(example) = complib_Hinf(example);
    catch
        failed = [failed, example];
    end
end

save("sol_Hinf.mat", "outputs", "failed");

%% $H_2$ case (Table 3)
outputs = struct();
failed = [];
for example = examples_to_run
    clear('yalmip');    % Cleanup YALMIP backend
    try
        outputs.(example) = complib_H2(example);
    catch
        failed = [failed, example];
    end
end

save("sol_H2.mat", "outputs", "failed");