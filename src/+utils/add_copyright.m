function add_copyright(target, year, name, type, options)
    %ADD_COPYRIGHT Add copyright info to `.m` files in the choosen directory
    % 
    %    ADD_COPYRIGHT() adds copyright to all `.m` files in current directory
    %    with current year, default name and default copyright type.
    % 
    %    ADD_COPYRIGHT(target) adds copyright to all `.m` files in target
    %    directory or in target `.m` file with current year, default author name
    %    and default copyright type.
    % 
    %    ADD_COPYRIGHT(..., year, name, type) adds copyright with specified
    %    year, author name and copyright type.
    %
    %   ------------------------------------------------------------------------
    %   Description:
    %       Adds copyright info to the `.m` files in the current directory and
    %       all the subfolders. The copyright line looks like,
    %           %   Copyright <YEAR> <NAME> (<TYPE> License)
    %       e.g.
    %           %   Copyright 2022 John Doe (MIT License)
    % 
    %   ------------------------------------------------------------------------
    %   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
    %
    
    arguments
        target = []
        year (1, 1) string = ""
        name (1, 1) string = "Vaibhav Gupta, DDMAC, EPFL"
        type (1, 1) string = "MIT"
        options.recursive (1, 1) logical = false
        options.verbosity (1, 1) int32 {mustBeMember(options.verbosity, [0, 1, 2])} = 1
    end
    
    logger = @(type, varargin) utils.logger_verbose(options.verbosity, type, varargin{:});
    logger("START", mfilename);
    cleanUp = onCleanup(@() logger("STOP", mfilename));
    
    curdir = pwd;
    if isempty(target)
        target = curdir;
    end
    
    if strcmpi(year, "")
        year = string(datetime("today", "Format", "yyyy"));
    end
    
    copyright_string = sprintf("%%   Copyright %s %s (%s License)\n", year, name, type);

    
    if exist(target, "file") == 2
        add_copyright_to_file(dir(target), copyright_string, logger);
    elseif exist(target, "dir") == 7
        [~, name, ~] = fileparts(target);
        logger("INFO", "Target is a dir! Adding copyright to `%s` and its subfolders ...", name);
        files = get_files(target, options.recursive);
        arrayfun(@(file) add_copyright_to_file(file, copyright_string, logger), files);
    else
        logger("WARN", "Target is a not a `.m` file or a folder! Exiting!", target);
    end
end

%% Helper Functions
function add_copyright_to_file(file_dir, copyright_string, logger)
%ADD_COPYRIGHT_TO_FILE Adds copyright info to the file
% 

    [~, name, ext] = fileparts(file_dir.name);

    % Skip over any file that is not a `.m` file or is "contents.m"
    if strcmpi(name, "contents") || ~strcmpi(ext, ".m")
        return          
    end
    logger("INFO", "Target is a file! Adding copyright to `%s` ...", name);

    org_file = fullfile(file_dir.folder, file_dir.name);
    tmp_file = fullfile(file_dir.folder, file_dir.name + ".tmp");

    %% Read lines and add/update copyright info
    fid_1 = fopen(org_file,'r+');
    fid_2 = fopen(tmp_file,'w');

    found_copyright = false;
    docstring_block = false;
    docstring_spacing = "";

    next_line = fgets(fid_1);
    while ischar(next_line)
        if ~found_copyright
            if ~docstring_block
                if startsWith(next_line, whitespacePattern(0, 80) + "%")
                    % Currently in docstring block
                    docstring_block = true;
                    docstring_spacing = extractBefore(next_line, "%");
                end
            else
                copyright_pattern = whitespacePattern(0, 80) + "%" + whitespacePattern(0, 80) + "Copyright";
                if startsWith(next_line, copyright_pattern, "IgnoreCase", true)
                    % Found a copyright in the docstring block
                    found_copyright = true;
    
                    % Replace line by copyright_string
                    next_line = docstring_spacing + copyright_string;
                elseif ~startsWith(next_line, whitespacePattern(0, 80) + "%")
                    % Exiting the docstring block
                    docstring_block = false;
    
                    n = 80 - length(docstring_spacing) - 4;
                    % Write the copyright info (with extra lines)
                    fprintf(fid_2, docstring_spacing + "%%   %s\n", repmat('-', 1, n));
                    fprintf(fid_2, docstring_spacing + "%s", copyright_string);
                    fprintf(fid_2, docstring_spacing + "%%\n");
                    found_copyright = true;
                end
            end
        end
        fprintf(fid_2, "%s", next_line);    % copy fid_1 into fid_2
        next_line = fgets(fid_1);
    end
    fclose(fid_1);
    fclose(fid_2);
    
    %% Move tmp file to replace original file
    moved = movefile(tmp_file, org_file, 'f');
    if ~moved
        delete(tmp_file)
        logger("WARN", "Could not move file! Copyright is not addded.");
    else
        logger("INFO", "Copyright added.");
    end
end

function files = get_files(folder, doRecursive)
    % All files & folder in root directory
    files = dir(folder);
    if isempty(files)
      return
    end

    % remove '.', '..'
    to_remove = arrayfun(@(x) strcmp(x.name, '.'), files) ...
        | arrayfun(@(x) strcmp(x.name, '..'), files);
    files = files(~to_remove);
    
    % subfolders
    isFolder = [files.isdir].';
    folders = files(isFolder)';

    % Root files
    files = files(~isFolder);
    
    if doRecursive
        for folder = folders
            files = [files; get_files(fullfile(folder.folder, folder.name), doRecursive)];
        end
    end
end
