function install_tbxmanager(options)
    %INSTALL_TBXMANAGER Install `tbxmanager` with the required submodules
    % 
    %   install_tbxmanager() installs `tbxmanager` with the base submodules.
    % 
    %   install_tbxmanager(..., "YALMIP", true) install YALMIP submodule.
    % 
    %   install_tbxmanager(..., "Sedumi", true) install Sedumi submodule.
    % 
    %   install_tbxmanager(..., "verbosity", verbosity_level) sets the verbosity
    %   level.
    % 
    %   ------------------------------------------------------------------------
    %   Verbosity Levels:
    %       * 0: No output
    %       * 1: Basic logs
    %       * 2: Complete logs with optimisation logs
    % 
    %   ------------------------------------------------------------------------
    %   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
    %

    arguments
        options.YALMIP (1, 1) logical = true
        options.Sedumi (1, 1) logical = false
        options.verbosity (1, 1) int32 {mustBeMember(options.verbosity, [0, 1, 2])} = 1
    end
    
    % Submodules to install
    submodules = {};
    if options.YALMIP
        submodules = [submodules, {'yalmip'}];
    end
    if options.Sedumi
        submodules = [submodules, {'sedumi'}];
    end

    logger = @(type, varargin) logger_verbose(options.verbosity, type, varargin{:});
    
    logger("START", "\n\t%s", ...
        "Installing toolbox manager (`tbxmanager`) ...", ...
        "Submodules being installed are:", ...
        strcat("    - ", submodules));
    
    % Cleanup -> Get back to the original directory
    current_dir = pwd;
    cleanUp = onCleanup(@() cleanUpFcn(logger, current_dir));

    %% Installation!!
    logger("INFO", "Choose the directory to install `tbxmanager`...");
    logger("INFO", "A new folder 'tbxmanager' is going to be created in the specified location.");
    logger("INFO", "If you do not specify the folder, the `tbxmanager` will be installed to the default user path.");

    % Get the desired installation folder
    c = uigetdir(userpath);
    if isequal(c, 0)
        logger("INFO", "No directory has been provided! Installing the `tbxmanager` in the default user directory...");
        c = userpath;
    end
 
    % Create a new directory in that folder
    d = fullfile(c, "tbxmanager");
    if isequal(exist(d, "dir"), 7)
        logger("WARN", sprintf("The installation directory '%s' already exists.", d));
        logger("WARN", "Please remove or rename the folder, or change the installation path. Assuming that `tbxmanager` is already installed!");
        return;
    end

    logger("INFO", "Creating the directory 'tbxmanager'...");
    out = mkdir(d);
    if ~out
        logger("ERROR", "An error appear when trying to create the folder '%s'.", d);
        return
    end

    % Enter that directory
    cd(d);

    % Download tbxmanager
    logger("INFO", "Downloading `tbxmanager` from the internet...");
    f = websave("tbxmanager.m", "http://www.tbxmanager.com/tbxmanager.m");

    % Install all required modules
    tbxmanager('install', submodules{:});

    % Add path to tbxmanager
    logger("INFO", "Adding to MATLAB path...");
    addpath(d);

    % Save path for future
    logger("INFO", "Saving MATLAB path...");
    status = savepath;

    if status
        logger("INFO", "Could not save the path to a default location, please provide alternate location...");
        c_path = uigetdir(pwd);
        if isequal(c_path,0)
            logger("INFO", "No directory specified, saving path to the current directory...");
            c_path = current_dir;
        end
        sn = savepath(fullfile(c_path, "pathdef.m"));
        if sn
            logger("ERROR", "Could not save the path automatically.");
            logger("ERROR", "Please, open the 'Set Path' button in the Matlab menu and save the path manually to some location.");
        end
    end
    
    logger("IMP", "`tbxmanager` successfully installed!");
end

%% Internal Functions
function logger_verbose(verbosity, type, varargin)
    % LOGGER_VERBOSE Helper function for logging with verbosity level checks
    % 
    %   LOGGER_VERBOSE(verbosity, type, vargin) logs based on verbosity level
    %   requirements and type.
    % 

    % START, STOP, and IMP Print (Prints always)
    switch type
        case "START"
            fprintf("%s\n", repmat('-', 80, 1));
            if length(varargin) == 1
                fprintf("[%5s] Running `%s` ... \n", "IMP", varargin{:});
            elseif length(varargin) > 1
                fprintf("[%5s] %s\n", "IMP", sprintf(varargin{:}));
            end
        case "STOP"
            if length(varargin) == 1
                fprintf("[%5s] `%s` finished. \n", "IMP", varargin{:});
            elseif length(varargin) > 1
                fprintf("[%5s] %s\n", "IMP", sprintf(varargin{:}));
            end
            fprintf("%s\n", repmat('-', 80, 1));
        case "IMP"
            fprintf("[%5s] %s\n", "IMP", sprintf(varargin{:}));
    end
    
    % Print nothing at verbosity level 0
    switch verbosity
        case 0
            % Only print errors
            if strcmpi(type, "ERROR")
                fprintf("[%5s] %s\n", type, sprintf(varargin{:}));
            end
        case 1
            % Print errors & infos
            if any(strcmpi(type, ["ERROR", "INFO", "WARN"]))
                fprintf("[%5s] %s\n", type, sprintf(varargin{:}));
            end
        case 2
            % Print everything
            fprintf("[%5s] %s\n", type, sprintf(varargin{:}));
    end
end

function cleanUpFcn(logger, current_dir)
    logger("STOP");
    cd(current_dir);
end