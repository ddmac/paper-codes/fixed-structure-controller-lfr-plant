function G_lft = load_COMPlib_plant(example_name)
    %LOAD_COMPLIB_PLANT Load COMPlib plant in LFR formulation
    % 
    %   This function loads the specified COMPLlib plant in LFR formulation.
    %   Check COMPlib manual for available examples.
    % 
    %   G_lft = load_COMPlib_plant(example_name)
    % 
    %   ------------------------------------------------------------------------
    %   Description
    %     Loads COMPlib plant `ex' which is defined as:
    %           * (d/dt)x(t) =  A*x(t) +  B1*w(t) +   B*u(t)
    %           *       z(t) = C1*x(t) + D11*w(t) + D12*u(t)
    %           *       y(t) =  C*x(t) + D21*w(t)
    %     where,
    %       * x: state
    %       * u: control
    %       * w: noise
    %       * z: regulation output
    %       * y: measurement
    % 
    %   ------------------------------------------------------------------------
    %   References
    %     [1] F. Leibfritz. COMPleib: Constrained matrix optimization problem
    %     library. 2006. Online at http://www.compleib.de.
    % 
    %   ------------------------------------------------------------------------
    %   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
    %

    arguments
        example_name (1, 1) string
    end

    if strcmpi(example_name, "TWO_AREA_PROBLEM")
        %% Original two-area problem (PSM in COMPlib)
        %  [1] C. E. Fosha and O. I. Elgerd, "The Megawatt-Frequency Control
        %  Problem: A New Approach Via Optimal Control Theory," in IEEE
        %  Transactions on Power Apparatus and Systems, vol. PAS-89, no. 4, pp.
        %  563-577, April 1970. doi: 10.1109/TPAS.1970.292603.
        %
        
        Pr1 = 2e3; % [MW]
        Pr2 = 2e3; % [MW]
        a12 = - Pr1 / Pr2;
        
        f = 60; % [Hz]
        T12 = 0.545; % [pu MW]
        H1 = 5; % [s]
        H2 = 5; % [s]
        D1 = 8.33e-3; % [pu MW/Hz]
        D2 = 8.33e-3; % [pu MW/Hz]
        Tt1 = 0.3; % [s]
        Tt2 = 0.3; % [s]
        Tgv1 = 0.08; % [s]
        Tgv2 = 0.08; % [s]
        R1 = 2.4; % [Hz/pu MW]
        R2 = 2.4; % [Hz/pu MW]
        
        A = [...
            [0,  T12    ,  0    ,  0,  0, -T12    ,  0   ,  0,  0]           ;   % \int{\Delta P_tie1}
            [0,  0      ,  1    ,  0,  0,  0      ,  0   ,  0,  0]           ;   % \int{\Delta f_1}
            [0, -T12    , -D1   ,  1,  0,  T12    ,  0   ,  0,  0] * f/(2*H1);   % \Delta f_1
            [0,  0      ,  0    , -1,  1,  0      ,  0   ,  0,  0] * 1/Tt1   ;   % \Delta P_g1
            [0,  0      , -1/R1 ,  0, -1,  0      ,  0   ,  0,  0] * 1/Tgv1  ;   % \Delta X_gv1
            [0,  0      ,  0    ,  0,  0,  0      ,  1   ,  0,  0]           ;   % \int{\Delta f_2}
            [0, -a12*T12,  0    ,  0,  0,  a12*T12, -D2  ,  1,  0] * f/(2*H2);   % \Delta f_2
            [0,  0      ,  0    ,  0,  0,  0      ,  0   , -1,  1] * 1/Tt2   ;   % \Delta P_g2
            [0,  0      ,  0    ,  0,  0,  0      , -1/R2,  0, -1] * 1/Tgv2  ;   % \Delta X_gv2
            ];
        B = [...                % u = [ \Delta P_c1, \Delta P_c2 ]
            0       , 0     ;
            0       , 0     ;
            0       , 0     ;
            0       , 0     ;
            1/Tgv1  , 0     ;
            0       , 0     ;
            0       , 0     ;
            0       , 0     ;
            0       , 1/Tgv2;
            ];
        B1 = [...               % w = [ \Delta P_d1, \Delta P_d2 ]
            0        , 0        ;
            0        , 0        ;
            -f/(2*H1), 0        ;
            0        , 0        ;
            0        , 0        ;
            0        , 0        ;
            0        , -f/(2*H2);
            0        , 0        ;
            0        , 0        ;
            ];
        C = eye(9);
        C1 = [...
            zeros(2, 9);
            1, 0  , 0, 0, 0,  0  , 0, 0, 0;
            0, 0  , 1, 0, 0,  0  , 0, 0, 0;
            0, 0  , 0, 0, 0,  0  , 1, 0, 0;
            0, 1  , 0, 0, 0,  0  , 0, 0, 0;
            0, 0  , 0, 0, 0,  1  , 0, 0, 0;
            0, T12, 0, 0, 0, -T12, 0, 0, 0;
        ];
        D12 = [eye(2); zeros(6, 2)];
        
        nu = size(B, 2);
        nw = size(B1, 2);
        ny = size(C, 1);
        nz = size(C1, 1);
        
        D11 = zeros(nz, nw);
        D21 = zeros(ny, nw);
    else
        %% Use COMPlib library
        oldpath = addpath("COMPlib_r1_0");
        c = onCleanup(@() path(oldpath));
        [A,B1,B,C1,C,D11,D12,D21,~,~,nu,~,ny] = COMPleib(example_name);
    end
    
    % Make LFR formulation
    G_lft = ss(A, [B1, B], [C1; C], [D11, D12; D21, zeros(ny,nu)]);
    G_lft = mktito(G_lft, ny, nu);
    
end

