function K0 = initial_hifoo_controller(P)
    %INITIAL_HIFOO_CONTROLLER Generates stabilising controller using HIFOO
    % 
    %   This function generates stabilising controller for the generalised
    %   plant represented in Linear Fraction Representation (LFR) form using
    %   HIFOO framework.
    % 
    %   K0 = initial_hifoo_controller(P)
    % 
    %   ------------------------------------------------------------------------
    %   References
    %     [1] D. Arzelier, G. Deaconu, S. Gumussoy, and D. Henrion, "H2 for
    %     HIFOO," in International Conference on Control and Optimization with
    %     Industrial Applications, Ankara, Turkey, Aug. 2011.
    % 
    %   ------------------------------------------------------------------------
    %   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
    %

    arguments
        P lti
    end

    oldpath = addpath("HIFOO3.501", "hanso2_01");
    c = onCleanup(@() path(oldpath));

    opts = struct();
    opts.prtlevel = 0;
    opts.normtol = 1e-4;
    opts.reltol = 1e-4;
    
    [K0, ~, ~, ~] = hifoo(P, '+', opts);
end

