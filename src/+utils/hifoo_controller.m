function [K_hifoo, norm_hifoo, time_hifoo] = hifoo_controller(P, controller_order, controller_type, K0)
    %HIFOO_CONTROLLER Generates controller using HIFOO
    % 
    %   This function generates $H_{2/\infty}$ controller for the generalised
    %   plant represented in Linear Fraction Representation (LFR) form using
    %   HIFOO framework.
    % 
    %   [K, norm, time] = hifoo_controller(P, controller_order, "Hinf") designs
    %   $H_\infty$ controller.
    % 
    %   [K, norm, time] = hifoo_controller(P, controller_order, "H2") designs
    %   $H_2$ controller.
    % 
    %   [K, norm, time] = hifoo_controller(..., K0) designs the controller
    %   hotstarted with K0.
    % 
    %   ------------------------------------------------------------------------
    %   References
    %     [1] D. Arzelier, G. Deaconu, S. Gumussoy, and D. Henrion, "H2 for
    %     HIFOO," in International Conference on Control and Optimization with
    %     Industrial Applications, Ankara, Turkey, Aug. 2011.
    % 
    %   ------------------------------------------------------------------------
    %   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
    %
    arguments
        P lti
        controller_order (1, 1) double
        controller_type (1, 1) string {mustBeMember(controller_type, ["H2", "Hinf"])}
        K0 lti = ss([])
    end

    oldpath = addpath("HIFOO3.501", "hanso2_01");
    c = onCleanup(@() path(oldpath));

    opts = struct();
    opts.prtlevel = 0;
    opts.normtol = 1e-4;
    opts.reltol = 1e-4;
    
    if strcmpi(controller_type, "H2")
        if isempty(K0)
            tStart = tic;
            [K_hifoo, ~, ~, ~] = hifoo(P, controller_order, 't', opts);
            time_hifoo = toc(tStart);
        else
            tStart = tic;
            [K_hifoo, ~, ~, ~] = hifoo(P, controller_order, K0, 't', opts);
            time_hifoo = toc(tStart);
        end
        CL_optimal_struct_YX = lft(P, K_hifoo);
        norm_hifoo = norm(CL_optimal_struct_YX, 2);
    elseif strcmpi(controller_type, "Hinf")
        if isempty(K0)
            tStart = tic;
            [K_hifoo, ~, ~, ~] = hifoo(P, controller_order, 'h', opts);
            time_hifoo = toc(tStart);
        else
            tStart = tic;
            [K_hifoo, ~, ~, ~] = hifoo(P, controller_order, K0, 'h', opts);
            time_hifoo = toc(tStart);
        end        
        CL_optimal_struct_YX = lft(P, K_hifoo);
        norm_hifoo = norm(CL_optimal_struct_YX, inf);
    end
end

