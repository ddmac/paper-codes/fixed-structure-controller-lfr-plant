function [K_systune, norm_systune] = systune_controller(G_lft, controller_order, controller_type)
    %SYSTUNE_CONTROLLER Generates controller using MATLAB's systune command
    % 
    %   This function generates $H_{2/\infty}$ controller for the generalised
    %   plant represented in Linear Fraction Representation (LFR) form using
    %   MATLAB's systune command.
    % 
    %   [K, norm] = systune_controller(P, controller_order, "Hinf") designs
    %   $H_\infty$ controller.
    % 
    %   [K, norm] = systune_controller(P, controller_order, "H2") designs
    %   $H_2$ controller.
    % 
    %   ------------------------------------------------------------------------
    %   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
    %

    arguments
        G_lft
        controller_order (1, 1) double
        controller_type (1, 1) string {mustBeMember(controller_type, ["H2", "Hinf"])}
    end

    ny = length(G_lft.OutputGroup.Y2);
    nu = length(G_lft.InputGroup.U2);

    K0  = tunableSS("K", controller_order, nu, ny);

    CL0 = lft(G_lft, K0);
    CL0.InputName  = 'w';
    CL0.OutputName = 'z';
    
    opts = systuneOptions("Display", "off", "SoftTol", 1e-4);
    
    if strcmpi(controller_type, "H2")
        SoftReqs = TuningGoal.WeightedVariance('w', 'z', [], []);
        CL = systune(CL0, SoftReqs, [], opts);
        K_systune = ss(CL.Blocks.K);
        norm_systune = norm(CL, 2);
    elseif strcmpi(controller_type, "Hinf")
        SoftReqs = TuningGoal.WeightedGain('w', 'z', [], []);
        CL = systune(CL0, SoftReqs, [], opts);
        K_systune = ss(CL.Blocks.K);
        norm_systune = norm(CL, inf);
    end
end