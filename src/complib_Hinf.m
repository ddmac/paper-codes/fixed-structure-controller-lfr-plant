function outputs = complib_Hinf(example_name, options)
    %COMPLIB_HINF Computes $H_\infty$ controllers for a COMPlib plant
    %   
    %   This function loads the COMPlib plant and compute fixed-order $H_\infty$
    %   controllers using various methods. It also computes the optimal
    %   $H_\infty$ controller.
    % 
    %   outputs = COMPLIB_HINF(example_name)
    % 
    %   outputs = COMPLIB_HINF(..., "factorisation", factorisation) selects the
    %   controller factorisation to be used.
    %       Possible factorisation are "auto", "Left", and "Right".
    % 
    %   outputs = COMPLIB_HINF(..., "maxIter", maxIter) selects the maximum
    %   number of controller iterations.
    % 
    %   ------------------------------------------------------------------------
    %   Output
    %     It is a structure with fields:
    %       * optimal      : `hinfsyn` command of MATLAB (Optimal)
    %       * systune      : `systune` command of MATLAB
    %       * hifoo        : `HIFOO`
    %       * hifoo_initial: `HIFOO` hotstarted with same stable controller
    %       * datadriven   : Proposed approach in Schuchert et. al. (2013)
    % 
    %       Each field is a structure containting the information about the
    %       controller synthesis using the chosen method,
    %         * K       : Synthesised controller
    %         * norm    : Closed-loop norm
    %         * compTime: Computation time
    % 
    %   ------------------------------------------------------------------------
    %   References
    %     [1] F. Leibfritz. COMPleib: Constrained matrix optimization problem
    %     library. 2006. Online at http://www.compleib.de.
    % 
    %     [2] D. Arzelier, G. Deaconu, S. Gumussoy, and D. Henrion, "H2 for
    %     HIFOO," in International Conference on Control and Optimization with
    %     Industrial Applications, Ankara, Turkey, Aug. 2011.
    % 
    %     [3] P. Schuchert, V. Gupta, and A. Karimi, “Data-driven fixed-structure 
    %     frequency-based $\mathcal{H}_2$ and $\mathcal{H}_\infty$ controller 
    %     design,” Automatica, vol. 160, p. 111398, Feb. 2024, 
    %     doi: 10.1016/j.automatica.2023.111398.
    % 
    %   ------------------------------------------------------------------------
    %   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
    %

    arguments
        % COMPlib example to run
        example_name (1, 1) string
        
        % Controller factorisation method
        options.factorisation (1, 1) string ...
            {mustBeMember(options.factorisation, ["auto", "Left", "Right"])} = "auto";
        
        % Maximum number of controller iterations
        options.maxIter (1, 1) double = 1000
    end

    %% Settings

    % Initializes Mersenne Twister generator with seed 0 (This is the default
    % setting at the start of each MATLAB session)
    rng(1234, "twister"); 
    
    % Desired controller order
    controller_order =  2;
    
    % ----- Logging ----- Log folder
    logs_folder = fullfile("Logs", "Hinf"); 
    if ~exist(logs_folder, "dir")
        mkdir(logs_folder);
    end
    
    % Log file
    fileID = fopen(fullfile(logs_folder, example_name+".log"), "w");
    c = onCleanup(@() fclose(fileID));
    fid = [fileID, 1];
    
    % Print handler
    printf = @(varargin) arrayfun(@(f) fprintf(f, "%-80s\n", varargin{:}), ...
        fid, "UniformOutput", 0);
    
    % Default specifications
    W = logspace(-4, 4, 101);
    objectiveOpts = {};
    
    % Special cases
    switch example_name
        case "JE1"
            objectiveOpts = {"epsNormalisation", 1e-8, "epsSVD", 1e-4};
    end

    %% Load Plant
    P = utils.load_COMPlib_plant(example_name);
    nz = length(P.OutputGroup.Y1);
    ny = length(P.OutputGroup.Y2);
    nw = length(P.InputGroup.U1);
    nu = length(P.InputGroup.U2);
        
    Pf = frd(P, W);

    %% Auto factorisation selection
    if strcmpi(options.factorisation, "auto")
        right_feasible = (ny <= nw);
        left_feasible = (nu <= nz);
        right_prefer = (ny <= nu);
    
        switch (left_feasible*4 + right_feasible*2 + right_prefer) % logic table
            case {2, 3, 7}
                % [010, 011, 111]
                options.factorisation = "right";
            case {4, 5, 6}
                % [100, 101, 110]
                options.factorisation = "left";
            otherwise
                % [000, 001]
                printf(sprintf("No possible factorisation for example `%s`!", example_name));
                error("No possible factorisation!");
        end
    end

    printf(sprintf("Using %s factorisation for example `%s`!", options.factorisation, example_name));

    %% Initial controller
    K = datadriven.Controller.SS(controller_order, ny, nu, 0, options.factorisation);

    % If stable, use zero controller (default)
    if ~isstable(P)
        K0 = utils.initial_hifoo_controller(P);
        K.setinitial(K0);
    end

    %% Other Controllers 
    
    % Optimal controller
    opts_hinfsyn = hinfsynOptions("RelTol", 1e-12, "AbsTol", 1e-12);
    tic;
    [K_optimal, ~, norm_optimal] = hinfsyn(P, ny, nu, opts_hinfsyn);
    t_optimal = toc();
    outputs.optimal = struct( ...
        "K", K_optimal, ...
        "norm", norm_optimal, ...
        "compTime", t_optimal);
    
    % Systune controller
    tic;
    [K_systune, norm_systune] = utils.systune_controller(P, controller_order, "Hinf");
    t_systune = toc();
    outputs.systune = struct( ...
        "K", K_systune, ...
        "norm", norm_systune, ...
        "compTime", t_systune);

    % HIFOO controllers
    [K_hifoo_init, norm_hifoo_init, time_hifoo_init] = utils.hifoo_controller(P, controller_order, "Hinf", ss(K));
    outputs.hifoo_initial= struct( ...
        "K", K_hifoo_init, ...
        "norm", norm_hifoo_init, ...
        "compTime", time_hifoo_init);
    
    [K_hifoo, norm_hifoo, time_hifoo] = utils.hifoo_controller(P, controller_order, "Hinf");
    outputs.hifoo = struct( ...
        "K", K_hifoo, ...
        "norm", norm_hifoo, ...
        "compTime", time_hifoo);

    %% Datadriven Controller

    % Initialize optimisation logger
    logger = utils.Logger(fid, ...
        "Norm"    , "% 8.4f"  , ...
        "Obj."    , "% 8.4f"  , ...
        "Max Eig.", "% 8.2f"  , ...
        "Time"    , "%7.3f"  , ...
        "Status"  , "%-40s"   );
    logger.init();
    
    % Sets up initial properties
    Kc = ss(K);
    curr_norm = Inf;
    solverTime = 0;
    prev_obj  = Inf;

    for iter = 1:options.maxIter
        logger.log_iter(iter);
        
        try
            % Create objective
            [LMIs, OBJ] = datadriven.Objective.Hinf(Pf, K, W, objectiveOpts{:});
        catch e
            % Quit on error while making objective
            logger.log_err(strcat(...
                "Objective Error! => ", ...
                e.message));
            logger.cleanup();
            error("Objective Error");
        end
    
        % Controller optimisation
        opts = sdpsettings('solver', 'mosek', 'verbose', 0);
        JOB = optimize(LMIs, OBJ, opts);
        
        if JOB.problem ~= 0
            % Quit on optimisation failure
            logger.log_err(strcat(...
                "Optimization Failed! => ", ...
                JOB.info));
            break;
        end
        
        % Normalise and extract controller
        K.normalise();
        Kc = ss(K);
        curr_obj  = sqrt(double(OBJ));
        curr_max_eig = max(abs(eig(Kc)));
        
        CL_curr = minreal(lft(P, Kc), [], false);
        curr_norm = norm(CL_curr, inf);
        if max(real(eig(CL_curr))) > 1e-10
            % Hard quit on instability
            logger.log_err(strcat(...
                "Unstable closed-loop! => ", ...
                JOB.info));
            logger.cleanup();
            error("Unstable solution");
        end

        % Log the interation outputs
        logger.log(...
            curr_norm, ...
            curr_obj, ...
            curr_max_eig, ...
            JOB.solvertime, ...
            JOB.info);
        
        % Update computation time
        solverTime = solverTime + JOB.solvertime;
        
        % Check for convergence
        err = abs(curr_obj - prev_obj);
        if (err < 1e-4) || (err/prev_obj < 1e-4)
            break;
        end
        if curr_max_eig > 1e3
            break;
        end
        prev_obj = curr_obj;
    end
    logger.cleanup();
    
    outputs.datadriven = struct( ...
        "K", Kc, ...
        "norm", curr_norm, ...
        "compTime", solverTime, ...
        "iterTime", solverTime / iter);

    %% Print norms and computation times
    printf(...
        sprintf("\nClosed-loop norm for %s:", example_name), ...
        sprintf("    Optimal        : % 7.4f", outputs.optimal.norm), ...
        sprintf("    HIFOO          : % 7.4f", outputs.hifoo.norm), ...
        sprintf("    HIFOO (initial): % 7.4f", outputs.hifoo_initial.norm), ...
        sprintf("    `systune`      : % 7.4f", outputs.systune.norm), ...
        sprintf("    `datadriven`   : % 7.4f", outputs.datadriven.norm) ...
        );

    printf(...
        sprintf("\nComputation time for `datadriven` synthesis is % 7.2f s.", ...
            outputs.datadriven.compTime) ...
        );
    
end