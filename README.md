# Data-driven fixed-structure frequency-based $\mathcal{H}_2$ and $\mathcal{H}_\infty$ controller design

<!-- Badges -->
[![License: MIT][license-badge]](/LICENSE)
[![Maintainter: Vaibhav Gupta][maintainer-badge]](mailto:vaibhav.gupta@epfl.ch)
[![DOI][DOI-badge]][DOI_link]  

This repository contains the code that has been used for

```bibtex
@article{schuchert_DatadrivenFixedstructureFrequencybased_2024,
  title   = {Data-Driven Fixed-Structure Frequency-Based {$\mathcal{H}_2$} and {$\mathcal{H}_\infty$} Controller Design},
  author  = {Schuchert, Philippe and Gupta, Vaibhav and Karimi, Alireza},
  year    = {2024},
  month   = feb,
  journal = {Automatica},
  volume  = {160},
  pages   = {111398},
  doi     = {10.1016/j.automatica.2023.111398},
}
```

<!----------------------------------------------------------------------------->
## Requirements

- **MATLAB** (2019b or newer)
  - Control System Toolbox
  - Signal Processing Toolbox
  - Statistics and Machine Learning Toolbox
- **Toolbox manager** (`tbxmanager`) for MATLAB
  - yalmip
  - sedumi
- **MOSEK** version 9.2 or above
- **HIFOO** (HIFOO controller systhesis)
- **HANSO** (Hybrid Algorithm for Nonsmooth Optimization)
- **COMPleib** (COnstrained Matrix-optimization Problem library)

### Installation Instructions

- **`tbxmanager`**
  - Run [`utils.install_tbxmanager()`](src/+utils/install_tbxmanager.m) to install `tbxmanager` with all required submodules.
- **MOSEK**
  - Download appropiate installer from [MOSEK][mosek-download] website and install the software.
  - For academic uses, a [free license][mosek-academic] could be requested. For commercial purposes, a 30-day [trial license][mosek-commercial] is also available.

### Notes on Dependencies

Following libraries are provided with code for easier replication of the study.

- **HIFOO** (HIFOO controller systhesis)
  - Provided "as is".
  - License: GNU GPL v3.0
- **HANSO** (Hybrid Algorithm for Nonsmooth Optimization)
  - Provided "as is".
  - License: GNU GPL v3.0
- **COMPleib** (COnstrained Matrix-optimization Problem library)
  - COMPleib library for comparison of various models using HIFOO, SYSTUNE and the proposed approach.
  - Provided "as is".
  - License: http://www.friedemann-leibfritz.de/License/License_COMPlib.htm

<!----------------------------------------------------------------------------->
## Repository structure

The structure of the `src` folder in the repository is as follows:

```shell
  ┣ +datadriven       # Datadriven controller design package
  ┣ +utils            # Utility functions
  ┣ COMPlib_r1_0      # Folder for COMPelib Library
  ┣ hanso2_01         # Folder for HANSO Optimiser (for HIFOO)
  ┣ HIFOO3.501        # Folder for HIFOO Package
  ┣ complib_H2.m      # Function for H2 controller synthesis comparison
  ┣ complib_Hinf.m    # Function for Hinf controller synthesis comparison
  ┗ main_6_2.m        # Script for Section of the paper
```

<!----------------------------------------------------------------------------->
## Usage Instructions

Set the current folder to the `src` folder.

- To generate the data and table for the Section 6.2 ""Comparison with HIFOO and SYSTUNE", run `main_6_2.m`.

<!----------------------------------------------------------------------------->
<!-- Markdown Links -->
[license-badge]: https://img.shields.io/gitlab/license/ddmac/paper-codes/fixed-structure-controller-lfr-plant?label=License&gitlab_url=https%3A%2F%2Fgitlab.epfl.ch
[maintainer-badge]: https://img.shields.io/badge/Maintainer-Vaibhav_Gupta-blue.svg
[DOI-badge]: https://img.shields.io/badge/DOI-10.1016/j.automatica.2023.111398-blue.svg
[DOI_link]: https://doi.org/10.1016/j.automatica.2023.111398
[mosek-download]: https://www.mosek.com/downloads/
[mosek-academic]: https://www.mosek.com/products/academic-licenses/
[mosek-commercial]: https://www.mosek.com/products/trial/
